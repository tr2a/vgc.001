package vgc.com.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import vgc.com.entities.Seat;
import vgc.com.repositories.iMovieRepository;
import vgc.com.repositories.iSeatRepository;

@Service
public class SeatService {

    @Autowired
    iSeatRepository seatRepository;

    public ResponseEntity<?> getSeat() {
        return ResponseEntity.ok(seatRepository.findAll());
    }

    public ResponseEntity<?> getRoomBySeat(int id) {
        return ResponseEntity.ok(seatRepository.getOne(id).getRoom_id());

    }

    public ResponseEntity<?> getSeatType(int id) {
        return ResponseEntity.ok(seatRepository.getOne(id).getSeat_type());
    }

    public ResponseEntity<String> getSeatName(int id) {
        Seat seat = seatRepository.getOne(id);
        String s1 = String.valueOf(seat.getSeat_row());
        String s2 = String.valueOf(seat.getSeat_num());
        return ResponseEntity.ok(s1.concat(s2));
    }



}
