package vgc.com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import vgc.com.repositories.iUserRepository;

@Service
public class UserService {

    @Autowired
    private iUserRepository userRepository;


    public ResponseEntity<?> getUser(){
        return ResponseEntity.ok( userRepository.findAll());
    }

    public ResponseEntity<?> getUserFullName(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getFullname());
    }

    public ResponseEntity<?> getUserCity(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getCity());
    }

    public ResponseEntity<?> getUserName(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getUsername());
    }

    public ResponseEntity<?> getUserPhone(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getPhone());
    }

    public ResponseEntity<?> getUserBirthDay(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getBirthday());
    }
    public ResponseEntity<?> getUserEmail(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getEmail());
    }

    public ResponseEntity<?> getUserPoint(int id){
        return ResponseEntity.ok(userRepository.getOne(id).getPoint());
    }
}
