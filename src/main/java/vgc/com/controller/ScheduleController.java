package vgc.com.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import vgc.com.entities.Movie;
import vgc.com.entities.Schedule;
import vgc.com.model.ResponseData;
import vgc.com.model.ScheduleModel;
import vgc.com.service.CinemaService;
import vgc.com.service.MovieService;
import vgc.com.service.RoomService;
import vgc.com.service.ScheduleService;

import java.util.LinkedList;
import java.util.List;

@RestController
@Api("Schedule's API")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private MovieService movieService;

    private RoomService roomService;

    CinemaService cinemaService;

    @ApiOperation(value = "Get schedule list", response = Schedule.class, responseContainer = "List")
    @GetMapping("/schedule")
    public ResponseEntity<?> getSchedule() {
        return ResponseEntity.ok(scheduleService.showSchedule());
    }


    @ApiOperation(value = "get schedule of a movie")
    @GetMapping( "/schedule/movie/{id}")
    public ResponseEntity<?> getMovieSchedule(@PathVariable int id) {

        List<Schedule> list = scheduleService.showSchedule();

        for (Schedule schedule : list)
            if (schedule.getMovie_id() != id) {

                list.remove(schedule);
            }


        return ResponseEntity.ok(list);
    }

    @GetMapping("/schedule/{id}/room")
    public ResponseEntity<?> getScheduleRoom(@PathVariable int id) {
        return ResponseEntity.ok(scheduleService.getScheduleRoom(id));
    }


    @GetMapping("/schedule/{date}")
    public ResponseEntity<?> getScheduleByDate(@PathVariable String date) {
        List<Schedule> list = scheduleService.showSchedule();

        for (Schedule schedule : list) {
            if (schedule.getSchedule_date().compareTo(date) != 0) {
                list.remove(schedule);
            }
        }
        return ResponseEntity.ok(list);
    }


    @ApiOperation(value = "Get a schedule of a movie")
    @GetMapping("/movies/{id}/schedule")
    public ResponseEntity<?> getScheduleMovieDetail(@PathVariable int id){


        List<Schedule> list = scheduleService.showSchedule();

        for (Schedule schedule : list)
            if (schedule.getMovie_id() != id) {

                list.remove(schedule);
            }
        List<Object> models = new LinkedList<>();
        for (Schedule schedule : list){
            ScheduleModel model = new ScheduleModel();
            model.setRoom_id(schedule.getRoom_id());
            model.setCinema_id(roomService.getRomCinema(schedule.getRoom_id()));
            model.setCinema_name(cinemaService.getCinemaName(model.getCinema_id()));
            model.setMovie_format(movieService.getMovieFormat(schedule.getMovie_id()));
            model.setSchedule_start(schedule.getSchedule_start());
            model.setSchedule_end(schedule.getSchedule_end());
            model.setSchedule_date(schedule.getSchedule_date());
            model.setSchedule_id(schedule.getSchedule_id());
            model.setMovie_id(schedule.getMovie_id());
            model.setMovie_name(movieService.getMovieName(schedule.getMovie_id()));
            models.add(model);
        }
        ResponseData responseData = new ResponseData();
        responseData.setStatus(HttpStatus.FOUND);
        responseData.setMsg(HttpStatus.FOUND.toString());
        responseData.setData(models);
        return ResponseEntity.ok(responseData);

    }

}
