package vgc.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vgc.com.entities.Seat;

public interface iSeatRepository extends JpaRepository<Seat, Integer> {


}
