package vgc.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import vgc.com.entities.User;

public interface iUserRepository extends JpaRepository<User, Integer> {
}
