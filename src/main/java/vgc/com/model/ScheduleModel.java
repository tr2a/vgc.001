package vgc.com.model;


import vgc.com.entities.Movie;

public class ScheduleModel {

    private int schedule_id;
    private int movie_id;
    private String movie_name;
    private String schedule_date;
    private int room_id;
    private int cinema_id;
    private String cinema_name;
    private String movie_format;
    private String schedule_start;
    private String schedule_end;

    public ScheduleModel() {
    }

    public ScheduleModel(int schedule_id, int movie_id, String movie_name, String schedule_date, int room_id, int cinema_id, String cinema_name, String movie_format, String schedule_start, String schedule_end) {
        this.schedule_id = schedule_id;
        this.movie_id = movie_id;
        this.movie_name = movie_name;
        this.schedule_date = schedule_date;
        this.room_id = room_id;
        this.cinema_id = cinema_id;
        this.cinema_name = cinema_name;
        this.movie_format = movie_format;
        this.schedule_start = schedule_start;
        this.schedule_end = schedule_end;

    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public String getSchedule_date() {
        return schedule_date;
    }

    public void setSchedule_date(String schedule_date) {
        this.schedule_date = schedule_date;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getCinema_id() {
        return cinema_id;
    }

    public void setCinema_id(int cinema_id) {
        this.cinema_id = cinema_id;
    }

    public String getCinema_name() {
        return cinema_name;
    }

    public void setCinema_name(String cinema_name) {
        this.cinema_name = cinema_name;
    }

    public String getMovie_format() {
        return movie_format;
    }

    public void setMovie_format(String movie_format) {
        this.movie_format = movie_format;
    }

    public String getSchedule_start() {
        return schedule_start;
    }

    public void setSchedule_start(String schedule_start) {
        this.schedule_start = schedule_start;
    }

    public String getSchedule_end() {
        return schedule_end;
    }

    public void setSchedule_end(String schedule_end) {
        this.schedule_end = schedule_end;
    }
}
